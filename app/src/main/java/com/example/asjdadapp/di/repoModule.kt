package com.example.asjdadapp.di

import com.example.asjdadapp.data.repository.CheckConnectionRepositoryImpl
import com.example.asjdadapp.data.storage.CheckConnectionStorage
import com.example.asjdadapp.data.storage.CheckConnectionStorageRepository
import com.example.asjdadapp.domain.repository.CheckConnectionRepository
import com.example.asjdadapp.domain.useCase.CheckConnectionUseCase
import org.koin.dsl.module

val repoModule = module {
    single {
        CheckConnectionUseCase(get())
    }

    factory<CheckConnectionStorageRepository> {
        CheckConnectionStorage(get())
    }

    factory<CheckConnectionRepository> {
        CheckConnectionRepositoryImpl(get())
    }
}