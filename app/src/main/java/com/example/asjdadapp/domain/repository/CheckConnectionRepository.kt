package com.example.asjdadapp.domain.repository

interface CheckConnectionRepository {
    fun checkConnection(): Boolean
}