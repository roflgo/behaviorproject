package com.example.asjdadapp.domain.useCase

import com.example.asjdadapp.domain.repository.CheckConnectionRepository

class CheckConnectionUseCase(private val checkConnectionRepository: CheckConnectionRepository) {
    fun execute(): Boolean {
        return checkConnectionRepository.checkConnection()
    }
}