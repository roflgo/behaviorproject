package com.example.asjdadapp.data.storage

interface CheckConnectionStorageRepository {
    fun checkConnection(): Boolean
}